/*	Copyright (C) 2021 by CS2C --- All Rights Reserved
 *
 * SYSTEM NAME: file_control
 * SOURCE FILE NAME: file_ct_netlink.c
 * MODEL BLOCK NAME: file_ct.ko
 * OBJECT FILE NAME: file_ct_netlink.o
 * AUTHOR: zhubing
 * DATE WRITTEN: 2021.08.20
 * REVISION: v1.0
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <net/sock.h>
#include <net/netlink.h>
#include <linux/netlink.h>
#include "file_ct_netlink.h"

#define NETLINK_USER 22
/* 定义netlink协议类型，最大32 */
#define USER_MSG (NETLINK_USER + 1)

char data[MSG_LEN]={0}; /* 用于存放从核外得到的数据信息 */

static DEFINE_MUTEX(nl_mutex);  /*多线程互斥， 必需，有可能多个用户进程同时向本模块发消息*/

static void nl_lock(void)
{
    mutex_lock(&nl_mutex);
}
static void nl_unlock(void)
{
    mutex_unlock(&nl_mutex);
}

/* netlink 内核配置参数 */
struct netlink_kernel_cfg cfg = {
    .input = test_netlink_rcv,/* input 回调函数 */
};

static struct sock *netlink_sockfd = NULL;   /*模块内部全局的netlink套接字*/

/* FUNCTION NAME: test_netlink_rcv()
 * FUNCTION DESCRIPTION: 接收从核外netlink发送来的数据
 * VARIABLES USED:
 *  skb：指向数据缓存结构体的指针.
 *  nlh：指向消息体的指针.
 *  data：接收数据的数组.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值
 *  NLMSG_DATA():用于取得消息的数据部分的首地址
 */
void test_netlink_rcv(struct sk_buff *skb)
{
    struct nlmsghdr *nlh = NULL;    /* 定义消息体 */

    nl_lock();  /* 上锁 */
    if (skb->len >= nlmsg_total_size(0))
    {
        nlh = nlmsg_hdr(skb);   /* 获取到netlink报头 */
        memset(data,0,MSG_LEN);
        strncpy(data,NLMSG_DATA(nlh),MSG_LEN-1);/* 宏NLMSG_DATA(nlh)用于取得消息的数据部分的首地址 */
        pr_alert("[test_lsm_netlink] kernel receive data : %s\n", data);
    }
    nl_unlock(); /* 解锁 */
}

/* FUNCTION NAME: test_netlink_init()
 * FUNCTION DESCRIPTION: 初始化内核netlink
 * VARIABLES USED：
 *  netlink_sockfd:netlink套接字.
 */
int test_netlink_init(void)
{
    netlink_sockfd = netlink_kernel_create(&init_net, USER_MSG, &cfg);   /* 创建内核 socket */
    if (netlink_sockfd == NULL) {
        pr_alert("test netlink init error\n");
        return -1;
    }
    pr_alert("[test_lsm_netlink] test netlink init ok\n");
    return 0;
}

/* FUNCTION NAME: test_netlink_exit()
 * FUNCTION DESCRIPTION: 回收释放内核netlink
 * VARIABLES USED：
 *  netlink_sockfd:netlink套接字.
 */
void test_netlink_exit(void)
{
    netlink_kernel_release(netlink_sockfd);  /* 释放套接字 */
    netlink_sockfd = NULL;
    pr_alert("[test_lsm_netlink] test netlink exit\n");
}

