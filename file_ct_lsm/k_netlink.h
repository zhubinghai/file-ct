#ifndef _K_NETLINK_H
#define _K_NETLINK_H

#define MSG_LEN 125

void test_netlink_rcv(struct sk_buff *skb);  /* 回调函数 */

int send_msg(char *msg,  int u_pid);

void test_netlink_rcv(struct sk_buff *skb);

int test_netlink_init(void);

void test_netlink_exit(void);

#endif
