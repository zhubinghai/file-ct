/*	Copyright (C) 2021 by CS2C --- All Rights Reserved
 *
 * SYSTEM NAME: file_control
 * SOURCE FILE NAME: file_ct_lsm.c
* MODEL BLOCK NAME: file_ct.ko
 * OBJECT FILE NAME: file_ct_lsm.o
 * AUTHOR: zhubing
 * DATE WRITTEN: 2021.08.20
 * REVISION: v1.0
 */

#include <linux/lsm_hooks.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/init.h>
#include <linux/mount.h>
#include <linux/security_ops.h>
#include <linux/path.h>
#include <linux/slab.h>
#include <linux/binfmts.h>
#include <net/sock.h>
#include <net/netlink.h>
#include <linux/netlink.h>
#include "file_ct_netlink.h"

extern char data[MSG_LEN];  /* 外部变量，用来存放从核外得到的数据 */

/* 定义文件控制权限 */
#define NO_PERM '0'
#define MY_PERM_READ '1'
#define MY_PERM_WRITE '2'
#define MY_PERM_EXEC '3'
#define MY_PERM_RENAME '4'
#define MY_PERM_DELETE '5'
#define MY_PERM_CHMOD '6'

/* FUNCTION NAME: lsm_handle_data()
 * FUNCTION DESCRIPTION: 分析核外传来的数据，得到指定文件路径及权限
 * EXAMPLE: val=lsm_handle_data(data,filename,perm)
 * VARIABLES USED:
 *  data：指向从核外传来的数据的指针.
 *  filename：指向得到的文件路径的指针.
 *  perm：指定的文件权限.
 *  i：中间变量.
 * MACROS USED：
 *  NO_PERM：指定无需进行监控.
* PSEUDO CODE：得到核外指定的文件路径，如果有指定perm权限，则返回0，没有或者无数据信息返回-1.
*/
int lsm_handle_data(char *data,char *filename,int perm)
{
    int i=0;
    if(strlen(data) <=0)
        return -1;
    while(data[i]!=' ')
    {
        filename[i]=data[i];
        i++;
    }
    i++;

    if(data[i] == NO_PERM)
        return -1;

    while(data[i] != 0)
    {
        if(data[i] == perm)
            return 0;
        i++;
    }
    return -1;
}

/* FUNCTION NAME: getfullname()
 * FUNCTION DESCRIPTION: 得到文件的完整路径
 * EXAMPLE: fullname=getfullname(dentry)
 * VARIABLES USED:
 *  dentry：指向目录项结构体指针.
 *  buf：中间指针变量.
 *  fullname：指向得到的文件完整路径的指针.
 * PSEUDO CODE：根据目录项结构体指针dentry得到完整的文件路径名，失败饭NULL，成功返回指向文件路径名的指针.
 */
char* getfullname(struct dentry *dentry)
{
    char *buf=NULL;
    char *fullname=NULL;
    buf = (char *)kmalloc(PATH_MAX, GFP_KERNEL);
    if (buf == NULL) {
        pr_alert("%s: malloc memory failed\n", __func__);
        return NULL;
    }
    memset(buf,0,PATH_MAX);
    fullname=dentry_path_raw(dentry,buf, PATH_MAX);
    kfree(buf);
    return fullname;
}

/* FUNCTION NAME: test_file_permission()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对打开后的文件读写操作进行监控
 * VARIABLES USED：
 *  file：指向file结构体的指针.
 *  mask：要对该文件进行的操作.
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret_r：中间变量，存放调用的函数返回值.
 *  ret_w：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_READ：指定监控的读权限.
 *  MY_PERM_WRITE：指定监控的写权限.
 *  MAY_READ：监控的读操作.
 *  MAY_WRITE：监控的写操作.
 * PSEUDO CODE：对指定的文件的读写操作进行监控，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_file_permission(struct file *file, int mask)
{
    char tmp[MSG_LEN]={0};
    //char *name=file->f_path.dentry->d_name.name;
    char *fullname =getfullname(file->f_path.dentry);   /* 得到完整路径名*/
    int ret_r=lsm_handle_data(data,tmp,MY_PERM_READ); /* 得到指定文件名及监控权限 */
    int ret_w=lsm_handle_data(data,tmp,MY_PERM_WRITE); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)))
    {
        if( (mask == MAY_READ) && (ret_r == 0))
        {
            pr_alert("[test_lsm] %s 读操作阻止\n",fullname);
            return -1;
        }

        if( (mask == MAY_WRITE) && (ret_w == 0))
        {
            pr_alert("[test_lsm] %s 写操作阻止\n",fullname);
            return -1;
        }
    }
    return 0;
}

/* FUNCTION NAME: test_inode_rename()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对文件重命名操作进行监控
 * VARIABLES USED：
 *  old_dir：指向重命名前的inode结构体指针.
 *  old_dentry：指向重命名前的dentry结构体指针.
 *  new_dir：指向重命名后的inode结构体指针.
 *  new_dentry：指向重命名后的dentry结构体指针.
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_RENAME：指定监控的重命名权限.
 * PSEUDO CODE：对指定的文件及重命名操作进行控制，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_inode_rename(struct inode *old_dir, struct dentry *old_dentry,struct inode *new_dir,struct dentry *new_dentry)
{

    //char *name=old_dentry->d_name.name;
    char tmp[MSG_LEN]={0};
    char *fullname=getfullname(old_dentry);/* 得到完整路径名*/
    int ret=lsm_handle_data(data,tmp,MY_PERM_RENAME); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)) && (ret == 0))
    {
        pr_alert("[test_lsm] %s 重命名操作阻止\n",fullname);
        return -1;
    }
    return 0;
}

/* FUNCTION NAME: test_path_rename()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对文件重命名操作进行监控
 * VARIABLES USED：
 *  old_dir：指向重命名前的path结构体指针.
 *  old_dentry：指向重命名前的dentry结构体指针.
 *  new_dir：指向重命名后的path结构体指针.
 *  new_dentry：指向重命名后的dentry结构体指针.
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_RENAME：指定监控的重命名权限.
 * PSEUDO CODE：对指定的文件及重命名操作进行控制，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_path_rename(const struct path *old_dir, struct dentry *old_dentry, const struct path *new_dir, struct dentry *new_dentry)
{
    char tmp[MSG_LEN]={0};
    char *fullname=getfullname(old_dentry);
    int ret=lsm_handle_data(data,tmp,MY_PERM_RENAME); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)) && (ret == 0))
    {
        pr_alert("[test_lsm] %s 重命名操作阻止\n",fullname);
        return -1;
    }
    return 0;
}

/* FUNCTION NAME: test_inode_unlink()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对文件删除操作进行监控
 * VARIABLES USED：
 *  dir：指向inode结构体指针.
 *  dentry：执行dentry结构体指针.
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_DELETE：指定监控的删除权限.
 * PSEUDO CODE：对指定的文件及删除操作进行控制，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_inode_unlink(struct inode *dir, struct dentry *dentry)
{
    char tmp[MSG_LEN]={0};
    char *fullname=getfullname(dentry); /* 得到完整路径名*/
    int ret=lsm_handle_data(data,tmp,MY_PERM_DELETE); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)) && (ret == 0))
    {
        pr_alert("[test_lsm] %s 删除操作阻止\n",fullname);
        return -1;
    }
    return 0;
}

/* FUNCTION NAME: test_bprm_check_security()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对文件执行操作进行监控
 * VARIABLES USED：
 *  bprm：指向linux_binprm结构体指针.
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_EXEC：指定监控的执行权限.
 * PSEUDO CODE：对指定的文件及执行操作进行控制，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_bprm_check_security(struct linux_binprm *bprm)
{
    //char *name=bprm->filename;
    //char *name=bprm->file->f_path.dentry->d_name.name;
    char tmp[MSG_LEN]={0};
    char *fullname=getfullname(bprm->file->f_path.dentry);
    int ret=lsm_handle_data(data,tmp,MY_PERM_EXEC); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)) && (ret == 0))
    {
        pr_alert("[test_lsm] %s 执行操作阻止\n",fullname);
        return -1;
    }
    return 0;
}


/* FUNCTION NAME: test_path_chmod()
 * FUNCTION DESCRIPTION: 作为一个钩子函数，对文件修改权限操作进行监控
 * VARIABLES USED：
 *  path：指向该文件的path结构体指针.
 *  mode：文件的权限
 *  tmp：中间数组变量，存放核外指定的文件路径名.
 *  fullname：指向正监控的文件的完整路径名的指针.
 *  ret：中间变量，存放调用的函数返回值.
 * MACROS USED：
 *  MSG_LEN：数据长度的最大值.
 *  MY_PERM_CHMOD：指定监控的修改文件权限操作.
 * PSEUDO CODE：对指定的文件及修改权限操作进行控制，不是该指定文件及权限则返回0，表示允许，否则返回-1，不允许.
 */
int test_path_chmod(const struct path *path, umode_t mode)
{
    //char *name=path->dentry->d_name.name;
    char tmp[MSG_LEN]={0};
    char *fullname=getfullname(path->dentry);
    int ret=lsm_handle_data(data,tmp,MY_PERM_CHMOD); /* 得到指定文件名及监控权限 */
    if((!strncmp(fullname,tmp,MSG_LEN-1)) && (ret == 0))
    {
        pr_alert("[test_lsm] %s chmod操作阻止\n",fullname);
        return -1;
    }
    return 0;
}

static struct security_operations test_ops = {
    .name = "test_lsm",
    .file_permission = test_file_permission,
    .inode_rename = test_inode_rename,
    .inode_unlink = test_inode_unlink,
    .bprm_check_security = test_bprm_check_security,
    .path_chmod = test_path_chmod,
};

/* FUNCTION NAME: test_lsm_init()
 * FUNCTION DESCRIPTION: 初始化lsm模块
 * VARIABLES USED：
 *  ret:中间变量.
 */
static int test_lsm_init(void)
{
    int ret;
    ret = security_add_external_ops(&test_ops);
    test_netlink_init();
    pr_alert("[test_lsm] 已经挂载\n");
    if (ret < 0)
        return ret;
    return 0;
}

/* FUNCTION NAME: test_lsm_exit()
 * FUNCTION DESCRIPTION: 释放lsm模块
 * VARIABLES USED：
 *  ret:中间变量.
 */
static void test_lsm_exit(void)
{
    security_del_external_ops(&test_ops);
    test_netlink_exit();
    pr_alert("[test_lsm] 取消挂载\n");
}

module_init(test_lsm_init);
module_exit(test_lsm_exit);

