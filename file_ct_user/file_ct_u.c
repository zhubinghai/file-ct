/*	Copyright (C) 2021 by CS2C --- All Rights Reserved
 *
 * SYSTEM NAME: file_control
 * SOURCE FILE NAME: file_ct_u.c
 * OBJECT FILE NAME: file_ct.o
 * EXECUTABLE FILE NAME: file_ct
 * AUTHOR: zhubing
 * DATE WRITTEN: 2021.08.20
 * REVISION: v1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <linux/netlink.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>


#define NETLINK_USER 22
/* 定义netlink协议类型，最大32 */
#define USER_MSG (NETLINK_USER + 1)

#define MSG_LEN 125

#define MAX_PLOAD 125

/* FUNCTION NAME: handle_data()
 * FUNCTION DESCRIPTION: 处理由终端命令行得到的文件路径及权限，返回处理后的数据信息
 * EXAMPLE: data=handle_data(argc,argv)
 * VARIABLES USED:
 *  argc：number of command line arguments.
 *  argv：array of pointers to the command line.
 *  data_size：处理后的数据信息长度.
 *  i：中间变量.
 *  n：中间变量.
 *  data：指向处理后的数据的指针.
 *  tmp：中间字符数组变量.
 *  count:用于保证每个指令都最多只出现一次
 * MACROS USED:
 *  MSG_LEN：数据长度的最大值
* PSEUDO CODE：成功返回处理后的数据的首地址指针，失败返回NULL
 */
char* handle_data(int argc,char **argv)
{
    if(argc != 3)
        return NULL;
    int data_size=0;
    int i=0;
    int n=0;
    char *data=NULL;
    char tmp[MSG_LEN]={0};
    int count[6]={0,0,0,0,0,0};
    data_size = strlen(argv[1]) + 13;
    if(data_size > MSG_LEN)
        return NULL;

    strncpy(tmp,argv[2],MSG_LEN-1);
    data = (char *)malloc(data_size);
    memset(data,0,data_size);
    strncpy(data,argv[1],data_size-1);
    *(data+strlen(argv[1]))=' ';

    if(tmp[i] != '-')
            return NULL;
    if(tmp[i+1] == '0' && tmp[i+2] == 0)
    {
        *(data+strlen(argv[1])+1)='0';
        return data;
    }
    if(tmp[i+1] == '0'&& tmp[i+2] != 0)
        return NULL;
    while(1)
    {
        i+=2;
        if((tmp[i] == '-' || tmp[i] == 0) && tmp[i-1] != 0)
        {
            switch(tmp[i-1])
            {
                case 'r':
                    *(data+strlen(argv[1])+n+1)='1';
                    count[0]++;
                    break;
                case 'w':
                    *(data+strlen(argv[1])+n+1)='2';
                    count[1]++;
                    break;
                case 'x':
                    *(data+strlen(argv[1])+n+1)='3';
                    count[2]++;
                    break;
                case 'n':
                    *(data+strlen(argv[1])+n+1)='4';
                    count[3]++;
                    break;
                case 'd':
                    *(data+strlen(argv[1])+n+1)='5';
                    count[4]++;
                    break;
                case 'c':
                    *(data+strlen(argv[1])+n+1)='6';
                    count[5]++;
                    break;
                default:
                    return NULL;
            }
        }
        else
            return NULL;
        if(tmp[i] == 0)
            break;
        n++;
    }
    for(i=0;i<6;i++)
    {
        if(count[i]>1)
            return NULL;
    }
    return data;
}


int main(int argc ,char **argv)
{
    char *data=handle_data(argc,argv);
    if(data ==NULL)
    {
        printf("参数错误!!!\n");
        printf("用法如下 :\n");
        printf("file_ct [path_file] -[r][w][x][n][d][c] \n");
        printf("file_ct [path_file] -[r]-[w]-[x]-[n]-[d]-[c]\n");
        printf("file_ct [path_file] -[0]\n");
        printf("-r :读操作阻止\n");
        printf("-w :写操作阻止\n");
        printf("-x :执行操作阻止\n");
        printf("-n :重命名操作阻止\n");
        printf("-d :删除操作阻止\n");
        printf("-c :修改权限操作阻止\n");
        printf("-0 :不对该文件进行权限阻止\n");
        return -1;
    }

    struct sockaddr_nl src_addr, dest_addr; //定义neilink协议套接字地址

    int sockfd; /* 套接字描述符 */
    struct nlmsghdr *nlh = NULL;    /* 定义消息体 */

    /* 创建socket */
    sockfd = socket(AF_NETLINK, SOCK_RAW, USER_MSG);
    if (sockfd < 0 ) {
        perror("create socket error\n");
        return (-1);
    }

    /* 初始化客户端的neilink地址，源地址 */
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family =  AF_NETLINK;   /* neilink协议族 */
    src_addr.nl_pid = getpid();  /*此处的pid通常用进程ID，表示来自用户态的唯一进程*/
    src_addr.nl_groups = 0; /* 用以多播，不需要时置为0 */

    /*绑定*/
    if (bind(sockfd, (struct sockaddr *)&src_addr, sizeof(src_addr)) != 0) {
        perror("bind fail");
        close(sockfd);
        return (-1);
    }

    /* 初始化目的地址 */
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;   /* neilink协议族 */
    dest_addr.nl_pid = 0;   /* to kernel */
    dest_addr.nl_groups = 0;

    /*填写data*/
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PLOAD));    /* 分配包含头部分的消息长度的空间给消息体 */
    memset(nlh, 0, sizeof(struct nlmsghdr));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PLOAD);    /* Length of message including header 包含头部分的消息长度 */
    nlh->nlmsg_flags = 0;   /* Additional flags 消息标记 */
    nlh->nlmsg_type = 0;    /* Message content  消息状态 */
    nlh->nlmsg_seq = 0;     /* Sequence number */
    nlh->nlmsg_pid = src_addr.nl_pid;   /* Sending process port ID */

    memcpy(NLMSG_DATA(nlh), data, strlen(data));    /* 宏NLMSG_DATA(nlh)用于取得消息的数据部分的首地址 */

    /* 发送消息到内核 */
    int se = sendto(sockfd, nlh, nlh->nlmsg_len, 0, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_nl));
    if (se < 0) {
        perror("sendto error: ");
        close(sockfd);
        return (-1);
    }
   // printf("send msg:%s\n",(char *)NLMSG_DATA(nlh));

    free((void *)nlh);
    close(sockfd);
    free(data);

    return 0;
}
